export const ResponsedStatus = {
    INVALID_INPUT: {
        code: 1,
        message: 'Invalid input'
    },
    DATA_NOTFOUND: {
        code: 2,
        message: 'Data not found'
    }
}
