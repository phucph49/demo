export class FilterAccountDto {
  readonly username: string;
  readonly id: number;
}
