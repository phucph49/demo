import { IsNotEmpty } from 'class-validator'

export class CreateAccountDto {
  @IsNotEmpty()
  readonly username: string;

  @IsNotEmpty()
  readonly password: string;
  
  readonly fullname: string;
}
