import { EntitySchema } from 'typeorm';
import { AccountEntity } from '../entity';

export const AccountSchema = new EntitySchema<AccountEntity>({
  name: 'account',
  target: AccountEntity,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    fullname: {
      type: String,
    },
    password: {
      type: String,
    },
    username: {
      type: String,
    },
  },
});
